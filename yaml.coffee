yaml = require('js-yaml')
fs = require('fs')
c = require('compendium-js')

parse_model = (yml) ->
  tokens = yml['model'].split(' ')
  variables = []
  a = []

  for token in tokens
    if token[0] == '<'
      token = token[1..-2]
      [token, type] = if ':' in token then token.split(':') else [token, '']
      variables.push [token, type]
      a.push "variable_#{variables.length - 1}"
    else
      a.push token

  # we dont use types for now
  s = a.join ' '
  console.log s
  
  pattern = c.analyse(s)
  pattern: pattern,
  properties: yml['properties']
  frames: parse_frames(yml['frames'])
  variables: variables
  length: a.length

s = () ->
  s2 = 2
  s2

parse_frames = (frames) ->
  scenes = frames #.split('\n')
  left = 100
  a = Math.ceil((left * 1.0) / scenes.length)
  frame_collections = []
  for scene in scenes
    console.log isNaN(scene[scene.length - 1])
    if isNaN(scene[scene.length - 1])
      [scene, size] = [scene, Math.min(left, a)]
    else
      [scene..., size] = scene.split(' ')
      [scene, size] = [scene.join(' '), parseInt(size)]
    console.log scene
    left -= size
    frame_collections.push {frames: scene.trim(), size: size}
  frame_collections

try
  models = fs.readdirSync('models/')
  fs.readFile
  yml = parse_model(yaml.load(fs.readFileSync("models/#{model}"))) for model in models
  console.log([yml])
  fs.writeFileSync('src/loader.coffee', """
load_models = () ->
  models = JSON.parse('#{JSON.stringify([yml])}')
  new Models models

""")

catch e
  console.log e


