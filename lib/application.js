// Generated by CoffeeScript 1.10.0
(function() {
  var Match, Models, load_models,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  load_models = function() {
    var models;
    models = JSON.parse('[{"pattern":[{"language":"en","time":8,"length":7,"governor":1,"raw":"variable_0 drink variable_1","stats":{"words":7,"confidence":0.7142857142857143,"p_foreign":0,"p_upper":0,"p_cap":0,"avg_length":3.5714285714285716,"breakpoints":0},"profile":{"label":"neutral","sentiment":0,"emphasis":1,"amplitude":0,"politeness":0,"dirtiness":0,"types":[],"main_tense":"present","negated":false},"has_negation":false,"entities":[],"deps":{"subjects":[],"objects":[]},"tokens":[{"raw":"variable","norm":"variable","stem":"variabl","pos":"JJ","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":null,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":false,"plural":null,"singular":null,"entity":-1,"is_punc":false},"deps":{"master":1,"governor":false,"type":"amod","dependencies":[]}},{"raw":"_","norm":"_","stem":"_","pos":"NN","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":null,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":true,"plural":null,"singular":"_","entity":-1,"is_punc":false},"deps":{"master":null,"governor":true,"type":"unknown","dependencies":[0,2,3,5,6]}},{"raw":"0","norm":"0","stem":"0","pos":"CD","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":0,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":false,"plural":null,"singular":"0","entity":-1,"is_punc":false},"deps":{"master":1,"governor":false,"type":"unknown","dependencies":[]}},{"raw":"drink","norm":"drink","stem":"drink","pos":"NN","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":null,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":true,"plural":null,"singular":"drink","entity":-1,"is_punc":false},"deps":{"master":1,"governor":false,"type":"unknown","dependencies":[]}},{"raw":"variable","norm":"variable","stem":"variabl","pos":"JJ","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":null,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":false,"plural":null,"singular":null,"entity":-1,"is_punc":false},"deps":{"master":5,"governor":false,"type":"amod","dependencies":[]}},{"raw":"_","norm":"_","stem":"_","pos":"NN","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":null,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":true,"plural":null,"singular":"_","entity":-1,"is_punc":false},"deps":{"master":1,"governor":false,"type":"unknown","dependencies":[4]}},{"raw":"1","norm":"1","stem":"1","pos":"CD","profile":{"sentiment":0,"emphasis":1,"negated":false,"breakpoint":false},"attr":{"value":1,"acronym":false,"abbr":false,"is_verb":false,"tense":"present","infinitive":null,"is_noun":false,"plural":null,"singular":"1","entity":-1,"is_punc":false},"deps":{"master":1,"governor":false,"type":"unknown","dependencies":[]}}],"tags":["JJ","NN","CD","NN","JJ","NN","CD"]}],"properties":{"subject":{"t":"human","sentiment":"happy","height":70,"width":22},"object":{"t":"beverage","height":38,"width":8}},"frames":[{"frames":"@subject ____ @object","size":34},{"frames":"@subject:top _ @object","size":50},{"frames":"@subject ____","size":16}],"variables":[["subject","N"],["object","N"]],"length":3}]');
    return new Models(models);
  };

  Match = (function() {
    function Match(variables1, model1, input) {
      var j, label, len, ref, variable;
      this.variables = variables1;
      this.model = model1;
      this.input = input;
      this.length = this.input.entities.length;
      ref = this.variables;
      for (variable = j = 0, len = ref.length; j < len; variable = ++j) {
        label = ref[variable];
        this.variables[label] = {
          input: variable
        } + this.model.properties[label];
      }
    }

    return Match;

  })();

  Models = (function() {
    function Models(models1) {
      this.models = models1;
    }

    Models.prototype.match = function(tree) {
      var j, len, m, model;
      for (j = 0, len = models.length; j < len; j++) {
        model = models[j];
        m = match_model(model, tree);
        if (m) {
          return m;
        }
      }
      return false;
    };

    Models.prototype.match_model = function(model, tree) {
      var i, j, len, model_child, ref, ref1, tree_child, variables;
      variables = {};
      if (model.pattern.root.raw !== tree.root.raw) {
        return false;
      }
      if (model.length !== tree.entities.length) {
        return false;
      }
      ref = model.pattern.root.children;
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        model_child = ref[i];
        tree_child = [tree.root.left, tree.root.right][i];
        if (model_child.type !== tree_child.type) {
          return false;
        }
        if (ref1 = model_child.raw, indexOf.call(model.variables, ref1) >= 0) {
          variables[model_child.raw] = tree_child;
        }
      }
      return new Match(variables, model, tree);
    };

    return Models;

  })();

  $(document).ready(function() {
    var visualize;
    $('form').submit(function(e) {
      e.preventDefault();
      return visualize($('#sentence').val());
    });
    console.debug(Models);
    window.models = load_models();
    return visualize = function(text) {
      var a, i, j, len, match, ref, root, token;
      a = compendium.analyse(text);
      root = a[0].root;
      window.a = a[0];
      $('#a').css('display', 'none');
      $('#search').css('display', 'block');
      ref = a[0].tokens;
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        token = ref[i];
        if (0 === i) {
          token.raw = token.raw[0].toUpperCase() + token.raw.slice(1);
        }
        console.log(token.pos);
        $('#search').append("<span class='type-" + (token.pos.toLowerCase()) + "'>" + token.raw + " </span>");
      }
      if (root.type[0] === 'V') {
        $('#loading').css('display', 'block');
        return match = models.match(a[0]);
      } else {
        console.log(root);
        return console.log('no');
      }
    };
  });

}).call(this);
