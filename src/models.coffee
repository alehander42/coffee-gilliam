class Match
  constructor: (@variables, @model, @input) ->
    @length = @input.entities.length
    for label, variable in @variables
      @variables[label] = {input: variable} + @model.properties[label]

class Models
  constructor: (@models) ->

  match: (tree) ->
    for model in models
      m = match_model(model, tree)
      if m
        return m
    false
    # _.find(@models, (model) -> match_model(model, tree))

  match_model: (model, tree) ->
    variables = {}
    if model.pattern.root.raw != tree.root.raw
      return false
    if model.length != tree.entities.length
      return false
    for model_child, i in model.pattern.root.children
      tree_child = [tree.root.left, tree.root.right][i]
      if model_child.type != tree_child.type
        return false
      if model_child.raw in model.variables
        variables[model_child.raw] = tree_child
    new Match(variables, model, tree)
